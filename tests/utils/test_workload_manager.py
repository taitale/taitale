from taitale import taitale_env
from taitale.askap import cmodel


def test_slurm_workload_manager(capfd):
    env = taitale_env(
        runtime="native", workload_manager="slurm", dryrun=True, user_defaults=False
    )

    cmodel(
        env=env,
        workers=2,
        nodes=2,
        args={
            "gsm.database": "votable",
            "gsm.file": "1934-638-VO",
            "gsm.ref_freq": "1319.129.0MHz",
            "shape": "[512, 512]",
            "cellsize": "[1arcsec, 1arcsec]",
            "direction": "[19h39m25.027, -63.42.45.61, J2000]",
            "filename": "1934-638.model",
            "frequency": "1319.129.0MHz",
            "increment": "1.0MHz",
            "flux_limit": "8mJy",
            "stokes": "[I]",
            "nterms": "1",
            "output": "casa",
        },
    )

    out, err = capfd.readouterr()
    assert (
        out
        == """Cmodel.gsm.database = votable
Cmodel.gsm.file = 1934-638-VO
Cmodel.gsm.ref_freq = 1319.129.0MHz
Cmodel.shape = [512, 512]
Cmodel.cellsize = [1arcsec, 1arcsec]
Cmodel.direction = [19h39m25.027, -63.42.45.61, J2000]
Cmodel.filename = 1934-638.model
Cmodel.frequency = 1319.129.0MHz
Cmodel.increment = 1.0MHz
Cmodel.flux_limit = 8mJy
Cmodel.stokes = [I]
Cmodel.nterms = 1
Cmodel.output = casa

Starting cmodel
srun --nodes=2 --ntasks=2 cmodel -c taitale_cmodel.in > cmodel-2021-11-05-09-51-38.log
cmodel complete
"""
    )
