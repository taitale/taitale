from taitale.utils import Parset


def test_Parset_single():
    expected_single = """Cimager.gridder = WProjection
# Comment for subgroup
Cimager.gridder.snapshotimaging = false
"""
    # Single level
    single_level = Parset(prefix="Cimager")
    single_level.set("gridder", "WProjection")
    single_level.comment("Comment for subgroup")

    gridder = single_level.group("gridder")
    gridder("snapshotimaging", "false")

    single_ser = single_level.to_string()

    assert (
        single_ser == expected_single
    ), f"\nExpected:\n{expected_single} \nActual:\n{single_ser}"


def test_Parset_nested():
    # Nested
    expected_nested = """Cimager.gridder.WProject.wmax = 35000
Cimager.gridder.WProject.nwplanes = 257
Cimager.gridder.WProject.oversample = 7
Cimager.gridder.WProject.maxsupport = 512
Cimager.gridder.WProject.variablesupport = true
"""

    nested = Parset(prefix="Cimager")
    wproj = nested.group("gridder", "WProject")
    wproj("wmax", 35000)
    wproj("nwplanes", 257)
    wproj("oversample", 7)
    wproj("maxsupport", 512)
    wproj("variablesupport", True)

    nested_ser = nested.to_string()

    assert (
        nested_ser == expected_nested
    ), f"\nExpected:\n{expected_nested} \nActual:\n{nested_ser}"

    print("All assertions passed: __testParset__")


def test_Parset_deserialize():
    parset = Parset(prefix="sources")
    # PurePath(Path(__file__).parent, "./1934.all.cleancomps")
    parset.read_from_file("./tests/assets/1934.all.cleancomps")
    assert (
        parset.to_string()
        == """sources.names = 1934-638
sources.1934-638.direction = [19h39m25.0342, -63.42.45.623, J2000]
sources.src1a.flux.i = 14.0763
sources.src1a.direction.ra = 2.89784e-07
sources.src1a.direction.dec = 4.45557e-07
sources.src1a.shape.bmaj = 8.48762e-06
sources.src1a.shape.bmin = 6.34335e-06
sources.src1a.shape.bpa = 1.30058
sources.1934-638.components = [src1a]
"""
    )


def test_Parset_parse_string():
    raw_content = """sources.names = 1934-638
sources.src1a.flux.i = 14.0763
sources.src1a.direction.ra = 2.89784e-07
"""

    parset = Parset(prefix="sources")
    parset.parse_string(raw_content)

    assert parset.get("names") == "1934-638"
    assert parset.to_string() == raw_content


def test_Parset_without_prefix():
    parset = Parset()
    parset.set("msin", "test.MS")
    parset.set("mystep1.type", "mystep.stepa")
    parset.set("mystep2.type", "mystep.stepb")
    parset.set("steps", "[averager, mystep1, mystep2]")

    assert (
        parset.to_string()
        == """msin = test.MS
mystep1.type = mystep.stepa
mystep2.type = mystep.stepb
steps = [averager, mystep1, mystep2]
"""
    )


def test_Parset_without_prefix_nested():
    parset = Parset()

    parset.set("msin", "test.MS")
    mystep1 = parset.group("mystep1")
    mystep2 = parset.group("mystep2")
    mystep1("type", "mystep.stepa")
    mystep2("type", "mystep.stepb")
    parset.set("steps", "[averager, mystep1, mystep2]")

    assert (
        parset.to_string()
        == """msin = test.MS
mystep1.type = mystep.stepa
mystep2.type = mystep.stepb
steps = [averager, mystep1, mystep2]
"""
    )


def test_Parset_read_from_file():
    parset = Parset(prefix="Ccalapply")
    parset.read_from_file("tests/assets/CcalapplyParset.in")

    # Check if the parset is correctly read
    # Assert single value
    assert parset.get("calibaccess") == "table"

    # Assert whole content
    expected = """Ccalapply.calibaccess = table
Ccalapply.calibaccess_table = calibdata.tab
Ccalapply.calibrate_scalenoise = false
Ccalapply.calibrate_allowflag = false
"""
    assert parset.to_string() == expected


def test_Parset_read_from_file_override():
    parset = Parset(prefix="Ccalapply")
    parset.read_from_file("tests/assets/CcalapplyParset.in")

    parset.set("calibaccess", "file")

    assert parset.get("calibaccess") == "file"

    expected = """Ccalapply.calibaccess = file
Ccalapply.calibaccess_table = calibdata.tab
Ccalapply.calibrate_scalenoise = false
Ccalapply.calibrate_allowflag = false
"""
    assert parset.to_string() == expected
