import argparse

from taitale.utils import CliArgs


def test_CliArgs():
    expected = "--brief --datacolumn DATA --full test.MS"
    cli_args = CliArgs()
    cli_args.set("brief")
    cli_args.set("datacolumn", "DATA")
    cli_args.set("full")
    cli_args.set("test.MS", pType="positional")

    actual = cli_args.to_string()

    assert actual == expected, f"\nExpected:\n{expected} \nActual:\n{actual}"


def test_CliArgs_parse_string():
    raw_content = "--brief --datacolumn DATA --full test.MS"

    parser = argparse.ArgumentParser()
    parser.add_argument("--brief", action="store_true")
    parser.add_argument("--datacolumn")
    parser.add_argument("--full", action="store_true")
    parser.add_argument("positional")

    cli_args = CliArgs(parser=parser)
    cli_args.parse_string(raw_content)

    assert cli_args.to_string() == raw_content


def test_CliArgs_parse_string_without_parser():
    raw_content = "--brief --datacolumn DATA --full test.MS"
    cli_args = CliArgs()
    cli_args.parse_string(raw_content)

    assert cli_args.to_string() == raw_content


def test_CliArgs_():
    raw_content = "-n -s 1024 1024 test.MS"

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", action="store_true")
    parser.add_argument("-s", nargs=2)
    parser.add_argument("positional")

    cli_args = CliArgs(parser=parser, option_sep="-")
    cli_args.parse_string(raw_content)

    assert cli_args.to_string() == raw_content
