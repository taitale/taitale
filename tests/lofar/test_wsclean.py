from taitale import taitale_env
from taitale.lofar import wsclean


def test_wsclean(capfd, mocker):
    env = taitale_env(runtime="native", dryrun=True, user_defaults=False)

    wsclean(
        env=env,
        args="-no-update-model-required -verbose -reorder "
        "-size 4096 4096 -scale 2arcsec -pol QU -mgain 0.85 -niter 50000 "
        "-auto-threshold 3 -join-polarizations -squared-channel-joining -log-time "
        "-no-mf-weighting -name output input.ms",
    )

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting wsclean
wsclean -no-update-model-required -verbose -reorder \
-size 4096 4096 -scale 2arcsec -pol QU -mgain 0.85 -niter 50000 -auto-threshold 3 \
-join-polarizations -squared-channel-joining -log-time -no-mf-weighting -name output \
input.ms > wsclean-2021-11-05-09-51-38.log
wsclean complete
"""
    )
