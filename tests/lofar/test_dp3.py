from taitale import taitale_env
from taitale.lofar import dp3


def test_dp3(capfd):
    env = taitale_env(runtime="native", dryrun=True, user_defaults=False)

    dp3(
        env=env,
        args={
            "msin": "MWA-1052736496-averaged.ms",
            "steps": "[average]",
            "average.type": "averager",
            "average.timestep": "8",
            "msout": "MWA-1052736496-averaged-averaged.MS",
            "msout.datacolumn": "DATA",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """msin = MWA-1052736496-averaged.ms
steps = [average]
average.type = averager
average.timestep = 8
msout = MWA-1052736496-averaged-averaged.MS
msout.datacolumn = DATA

Starting DP3
DP3 -c taitale_dp3.in > DP3-2021-11-05-09-51-38.log
DP3 complete
"""
    )
