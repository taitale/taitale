import pytest

from taitale import taitale_env
from taitale.lofar import losoto


def test_losoto(capfd):
    env = taitale_env(runtime="native", dryrun=True, user_defaults=False)

    losoto(
        env=env,
        h5parm="solutions.h5",
        args={
            "soltab": "sol000/amplitude000",
            "operation": "PLOT",
            "axisInTable": "time",
            "axisInCol": "ant",
            "plotFlag": "True",
            "prefix": "amp_",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """soltab = sol000/amplitude000
operation = PLOT
axisInTable = time
axisInCol = ant
plotFlag = True
prefix = amp_

Starting LoSoTo
losoto solutions.h5 taitale_losoto.parset > LoSoTo-2021-11-05-09-51-38.log
LoSoTo complete
"""
    )


def test_losoto_missing_h5parm():
    env = taitale_env(runtime="native", dryrun=True, user_defaults=False)

    with pytest.raises(ValueError, match="h5parm parameter is required"):
        losoto(
            env=env,
            args={
                "soltab": "sol000/amplitude000",
                "operation": "PLOT",
            },
        )
