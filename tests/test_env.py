import pytest

from taitale.env import TaitaleEnvironment, taitale_env


def test_taitale_environment_context_manager():
    """Test that TaitaleEnvironment works correctly as a context manager."""
    with TaitaleEnvironment() as config:
        assert isinstance(config, dict)
        assert config["runtime"] == "container"
        assert config["workload_manager"] == "mpi"
        assert config["mpi"]["workload_exec"] == "mpiexec -n {workers} {cmd}"


def test_taitale_environment_override_config():
    """Test that configuration can be overridden through kwargs."""
    with TaitaleEnvironment(runtime="native", workload_manager="slurm") as config:
        assert config["runtime"] == "native"
        assert config["workload_manager"] == "slurm"


def test_invalid_environment():
    """Test that invalid environment raises ValueError."""
    with pytest.raises(ValueError, match="Unknown environment"):
        with TaitaleEnvironment(runtime="invalid"):
            pass


def test_invalid_workload_manager():
    """Test that invalid workload manager raises ValueError."""
    with pytest.raises(ValueError, match="Unknown workload_manager"):
        with TaitaleEnvironment(runtime="container", workload_manager="invalid"):
            pass


def test_taitale_env_function():
    """Test the taitale_env function works with the new context manager."""
    config = taitale_env(runtime="native", workload_manager="mpi")
    assert config["runtime"] == "native"
    assert config["workload_manager"] == "mpi"
