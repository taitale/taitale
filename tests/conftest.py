import pytest

from taitale import taitale_env


@pytest.fixture(autouse=True)
def test_taitale_env(mocker):
    mocker.patch("taitale.utils.wrapper.getstamp", return_value="2021-11-05-09-51-38")
    env = taitale_env(
        runtime="container",
        dryrun=True,
        user_defaults=False,
        engine="docker",
        image="docker.io/csirocass/yandasoft",
        tag="1.11.0-openmpi4",
    )
    yield env
