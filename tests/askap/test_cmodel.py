from taitale.askap import cmodel


def test_cmodel(test_taitale_env, capfd):
    cmodel(
        env=test_taitale_env,
        workers=2,
        args={
            "dataset": "1934-638.ms",
            "modelimage": "1934-638.model",
            "nchan": "16416",
            "singleoutputms": "true",
            "useweightsderived": "true",
            "visweightcutoff": "1e-4",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """Cmodel.dataset = 1934-638.ms
Cmodel.modelimage = 1934-638.model
Cmodel.nchan = 16416
Cmodel.singleoutputms = true
Cmodel.useweightsderived = true
Cmodel.visweightcutoff = 1e-4

Starting cmodel
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mpiexec -n 2 cmodel -c \
taitale_cmodel.in > cmodel-2021-11-05-09-51-38.log
cmodel complete
"""
    )
