from taitale.askap import imager
from taitale.utils import Parset


def test_Imager(test_taitale_env, capfd):
    imager(
        env=test_taitale_env,
        parset="tests/assets/ImagerParset.in",
        workers=4,
        args={
            "dataset": "1934-638-1.ms",
            "Images.Names": "image.1924-638-1",
            "restore": True,
            "Images.image.1924-638.direction": "[19h39m25.027, -63.42.45.61, J2000]",
        },
    )

    out, err = capfd.readouterr()
    assert (
        out
        == """Cimager.dataset = 1934-638-1.ms
Cimager.datacolumn = DATA
Cimager.imagetype = fits
Cimager.ScanNumber = 0
Cimager.Images.Names = image.1924-638-1
Cimager.Images.shape = [512, 512]
Cimager.Images.cellsize = [2arcsec, 2arcsec]
Cimager.Images.image.1924-638.direction = [19h39m25.027, -63.42.45.61, J2000]
Cimager.Images.image.1924-638.nchan = 1
Cimager.Images.image.1924-638.nterms = 2
Cimager.Images.writeAtMajorCycle = false
Cimager.nworkergroups = 3
Cimager.nchanpercore = 12
Cimager.usetmpfs = false
Cimager.tmpfs = /dev/shm
Cimager.solverpercore = false
Cimager.nwriters = 1
Cimager.combinechannels = true
Cimager.barycenter = false
Cimager.preconditioner.Names = [Wiener]
Cimager.preconditioner.preservecf = true
Cimager.preconditioner.Wiener.robustness = -0.5
Cimager.restore = true
Cimager.ncycles = 5
Cimager.threshold.masking = 0.9
Cimager.threshold.majorcycle = 0.035mJy
Cimager.threshold.minorcycle = [30%, 0.5mJy, 0.03mJy]
Cimager.calibrate = false
Cimager.solver = Clean
Cimager.solver.Clean.solutiontype = MAXBASE
Cimager.solver.Clean.verbose = false
Cimager.solver.Clean.decoupled = true
Cimager.solver.Clean.tolerance = 0.01
Cimager.solver.Clean.weightcutoff = zero
Cimager.solver.Clean.weightcutoff.clean = false
Cimager.solver.Clean.logevery = 50
Cimager.solver.Clean.detectdivergence = true
Cimager.solver.Clean.algorithm = BasisfunctionMFS
Cimager.solver.Clean.niter = 400
Cimager.solver.Clean.gain = 0.2
Cimager.solver.Clean.psfwidth = 256
Cimager.solver.Clean.scales = [0, 3, 10]
Cimager.gridder.snapshotimaging = true
Cimager.gridder.snapshotimaging.wtolerance = 5600
Cimager.gridder.snapshotimaging.longtrack = true
Cimager.gridder.snapshotimaging.clipping = 0.05
Cimager.gridder = WProject
Cimager.gridder.WProject.wmax = 35000
Cimager.gridder.WProject.nwplanes = 257
Cimager.gridder.WProject.oversample = 7
Cimager.gridder.WProject.maxsupport = 512
Cimager.gridder.WProject.variablesupport = true
Cimager.gridder.WProject.offsetsupport = true
Cimager.gridder.WProject.sharecf = true
Cimager.gridder.WProject.frequencydependent = true

Starting imager
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mpiexec -n 4 imager -c \
taitale_cimager.in > imager-2021-11-05-09-51-38.log
imager complete
"""
    )


def test_Imager_parset(test_taitale_env, capfd):
    parset = Parset(prefix="Cimager")
    parset.set("datacolumn", "DATA")
    parset.set("imagetype", "fits")

    gridder = parset.group("gridder", "WProject")
    gridder("wmax", 35000)
    gridder("nwplanes", 257)
    gridder("oversample", 7)
    gridder("maxsupport", 512)

    imager(env=test_taitale_env, parset=parset, workers=8)

    out, err = capfd.readouterr()
    assert (
        out
        == """Cimager.datacolumn = DATA
Cimager.imagetype = fits
Cimager.gridder.WProject.wmax = 35000
Cimager.gridder.WProject.nwplanes = 257
Cimager.gridder.WProject.oversample = 7
Cimager.gridder.WProject.maxsupport = 512

Starting imager
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mpiexec -n 8 imager -c \
taitale_cimager.in > imager-2021-11-05-09-51-38.log
imager complete
"""
    )
