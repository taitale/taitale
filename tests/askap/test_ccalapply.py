from taitale.askap import ccalapply


def test_ccalapply(test_taitale_env, capfd):
    ccalapply(
        env=test_taitale_env,
        args={
            "dataset": "1934-638.ms",
            "calibaccess": "table",
            "calibaccess.table": "1934-638.calib.tab",
            "calibaccess.table.maxbeam": "30",
            "calibrate.scalenoise": "true",
            "calibrate.allowflag": "false",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """Ccalapply.dataset = 1934-638.ms
Ccalapply.calibaccess = table
Ccalapply.calibaccess.table = 1934-638.calib.tab
Ccalapply.calibaccess.table.maxbeam = 30
Ccalapply.calibrate.scalenoise = true
Ccalapply.calibrate.allowflag = false

Starting ccalapply
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 ccalapply -c \
taitale_ccalapply.in > ccalapply-2021-11-05-09-51-38.log
ccalapply complete
"""
    )
