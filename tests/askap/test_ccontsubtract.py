from taitale.askap import ccontsubtract


def test_ccontsubtract(test_taitale_env, capfd):
    ccontsubtract(
        env=test_taitale_env,
        workers=2,
        args={
            "dataset": "10uJy_simtest.ms",
            "doSubtraction": "true",
            "doUVlin": "true",
            "uvlin.order": "2",
            "uvlin.harmonic": "0",
            "uvlin.width": "54",
            "uvlin.offset": "0",
            "uvlin.direction": "SUN",
        },
    )

    out, _ = capfd.readouterr()

    assert (
        out
        == """CContSubtract.dataset = 10uJy_simtest.ms
CContSubtract.doSubtraction = true
CContSubtract.doUVlin = true
CContSubtract.uvlin.order = 2
CContSubtract.uvlin.harmonic = 0
CContSubtract.uvlin.width = 54
CContSubtract.uvlin.offset = 0
CContSubtract.uvlin.direction = SUN

Starting ccontsubtract
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mpiexec -n 2 ccontsubtract \
-c taitale_ccontsubtract.in > ccontsubtract-2021-11-05-09-51-38.log
ccontsubtract complete
"""
    )
