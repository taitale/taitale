from taitale.askap import csimulator


def test_Simulator_sourcedef(test_taitale_env, capfd):
    csimulator(
        env=test_taitale_env,
        parset="tests/assets/SimulatorParset.in",
        args={
            "dataset": "1934-638.ms",
            "sources.definition": "./tests/assets/1934.all.cleancomps",
            "antennas.definition": "parsets/ASKAP36.antpos.in",
            "feeds.definition": "parsets/ASKAP1feeds.in",
            "simulation.referencetime": "[2007Mar07, UTC]",
            "observe.number": 1,
            "observe.scan0": "[1934-638, TestsBand, -0.25h, 0.25h]",
            "spws.names": "[TestsBand]",
            "spws.TestsBand": '[48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]',
            "gridder.snapshotimaging": "false",
            "gridding": "WProject",
            "gridding.WProject.wmax": "35000",
            "gridding.WProject.nwplanes": "257",
            "gridding.WProject.oversample": "7",
            "gridding.WProject.maxsupport": "512",
            "gridding.WProject.variablesupport": "true",
            "gridding.WProject.offsetsupport": "true",
            "gridding.WProject.sharecf": "true",
            "gridding.WProject.frequencydependent": "true",
            "simulation.blockage": "0.02",
            "noise": "true",
            "noise.variance": "0.9",
        },
    )

    out, err = capfd.readouterr()
    assert (
        out
        == """Csimulator.Simulator.stman_bucketsize = 1048576
Csimulator.Simulator.stman_tilenchan = 1
Csimulator.Simulator.duration = "0.25h"
Csimulator.Simulator.spw_name = TestsBand
Csimulator.Simulator.spw = [48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]
Csimulator.Simulator.simulation_blockage = 0.01
Csimulator.Simulator.simulation_elevationlimit = 18deg
Csimulator.Simulator.simulation_autocorrwt = 1.0
Csimulator.Simulator.simulation_usehourangle = True
Csimulator.Simulator.simulation_referencetime = [2021June27, UTC]
Csimulator.Simulator.simulation_integrationtime = 10.0s
Csimulator.Simulator.corrupt = false
Csimulator.Simulator.noise = false
Csimulator.Simulator.noise_seed1 = %w
Csimulator.Simulator.noise_variance = 1.0
Csimulator.Simulator.noise_Tsys = 750
Csimulator.Simulator.noise_efficiency = 0.7
Csimulator.Simulator.noise_rms = 0.01
Csimulator.dataset = 1934-638.ms
Csimulator.sources.definition = ./tests/assets/1934.all.cleancomps
Csimulator.antennas.definition = parsets/ASKAP36.antpos.in
Csimulator.feeds.definition = parsets/ASKAP1feeds.in
Csimulator.simulation.referencetime = [2007Mar07, UTC]
Csimulator.observe.number = 1
Csimulator.observe.scan0 = [1934-638, TestsBand, -0.25h, 0.25h]
Csimulator.spws.names = [TestsBand]
Csimulator.spws.TestsBand = [48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]
Csimulator.gridder.snapshotimaging = false
Csimulator.gridding = WProject
Csimulator.gridding.WProject.wmax = 35000
Csimulator.gridding.WProject.nwplanes = 257
Csimulator.gridding.WProject.oversample = 7
Csimulator.gridding.WProject.maxsupport = 512
Csimulator.gridding.WProject.variablesupport = true
Csimulator.gridding.WProject.offsetsupport = true
Csimulator.gridding.WProject.sharecf = true
Csimulator.gridding.WProject.frequencydependent = true
Csimulator.simulation.blockage = 0.02
Csimulator.noise = true
Csimulator.noise.variance = 0.9

Starting csimulator
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 csimulator -c \
taitale_csimulator.in > csimulator-2021-11-05-09-51-38.log
csimulator complete
"""
    )


def test_Simulator_sources(test_taitale_env, capfd):
    csimulator(
        env=test_taitale_env,
        parset="tests/assets/SimulatorParset.in",
        args={
            "dataset": "1934-638.ms",
            "sources.names": "['src']",
            "sources.comp1.flux.i": "1.0",
            "sources.comp1.direction.ra": "9.69627362219072e-06",
            "sources.comp1.direction.dec": "9.69627362219072e-06",
            "sources.comp1.shape.bmaj": "4.84813681109536e-06",
            "sources.comp1.shape.bmin": "4.84813681109536e-06",
            "sources.comp1.shape.bpa": "2.42406840554768e-06",
            "sources.src.direction": "[12h30m00.000, -45.00.00.000, J2000]",
            "sources.src.components": "['comp1']",
            "antennas.definition": "parsets/ASKAP36.antpos.in",
            "feeds.definition": "parsets/ASKAP1feeds.in",
            "simulation.referencetime": "[2007Mar07, UTC]",
            "observe.number": 1,
            "observe.scan0": "[src, TestsBand, -0.25h, 0.25h]",
            "spws.names": "[TestsBand]",
            "spws.TestsBand": '[48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]',
            "gridder.snapshotimaging": "false",
            "gridding": "WProject",
            "gridding.WProject.wmax": "35000",
            "gridding.WProject.nwplanes": "257",
            "gridding.WProject.oversample": "7",
            "gridding.WProject.maxsupport": "512",
            "gridding.WProject.variablesupport": "true",
            "gridding.WProject.offsetsupport": "true",
            "gridding.WProject.sharecf": "true",
            "gridding.WProject.frequencydependent": "true",
            "simulation.blockage": "0.02",
            "noise": "false",
        },
    )

    out, err = capfd.readouterr()
    assert (
        out
        == """Csimulator.Simulator.stman_bucketsize = 1048576
Csimulator.Simulator.stman_tilenchan = 1
Csimulator.Simulator.duration = "0.25h"
Csimulator.Simulator.spw_name = TestsBand
Csimulator.Simulator.spw = [48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]
Csimulator.Simulator.simulation_blockage = 0.01
Csimulator.Simulator.simulation_elevationlimit = 18deg
Csimulator.Simulator.simulation_autocorrwt = 1.0
Csimulator.Simulator.simulation_usehourangle = True
Csimulator.Simulator.simulation_referencetime = [2021June27, UTC]
Csimulator.Simulator.simulation_integrationtime = 10.0s
Csimulator.Simulator.corrupt = false
Csimulator.Simulator.noise = false
Csimulator.Simulator.noise_seed1 = %w
Csimulator.Simulator.noise_variance = 1.0
Csimulator.Simulator.noise_Tsys = 750
Csimulator.Simulator.noise_efficiency = 0.7
Csimulator.Simulator.noise_rms = 0.01
Csimulator.dataset = 1934-638.ms
Csimulator.sources.names = ['src']
Csimulator.sources.comp1.flux.i = 1.0
Csimulator.sources.comp1.direction.ra = 9.69627362219072e-06
Csimulator.sources.comp1.direction.dec = 9.69627362219072e-06
Csimulator.sources.comp1.shape.bmaj = 4.84813681109536e-06
Csimulator.sources.comp1.shape.bmin = 4.84813681109536e-06
Csimulator.sources.comp1.shape.bpa = 2.42406840554768e-06
Csimulator.sources.src.direction = [12h30m00.000, -45.00.00.000, J2000]
Csimulator.sources.src.components = ['comp1']
Csimulator.antennas.definition = parsets/ASKAP36.antpos.in
Csimulator.feeds.definition = parsets/ASKAP1feeds.in
Csimulator.simulation.referencetime = [2007Mar07, UTC]
Csimulator.observe.number = 1
Csimulator.observe.scan0 = [src, TestsBand, -0.25h, 0.25h]
Csimulator.spws.names = [TestsBand]
Csimulator.spws.TestsBand = [48, 1319.129.0MHz, 1.0MHz, "XX YY XY YX"]
Csimulator.gridder.snapshotimaging = false
Csimulator.gridding = WProject
Csimulator.gridding.WProject.wmax = 35000
Csimulator.gridding.WProject.nwplanes = 257
Csimulator.gridding.WProject.oversample = 7
Csimulator.gridding.WProject.maxsupport = 512
Csimulator.gridding.WProject.variablesupport = true
Csimulator.gridding.WProject.offsetsupport = true
Csimulator.gridding.WProject.sharecf = true
Csimulator.gridding.WProject.frequencydependent = true
Csimulator.simulation.blockage = 0.02
Csimulator.noise = false

Starting csimulator
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 csimulator -c \
taitale_csimulator.in > csimulator-2021-11-05-09-51-38.log
csimulator complete
"""
    )
