from taitale.askap import msconcat


def test_msconcat_example_1(test_taitale_env, capfd):
    msconcat(env=test_taitale_env, args="-o cat.ms sb1.ms sb2.ms sb3.ms")

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting msconcat
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 msconcat --output_file cat.ms \
sb1.ms sb2.ms sb3.ms > msconcat-2021-11-05-09-51-38.log
msconcat complete
"""
    )
