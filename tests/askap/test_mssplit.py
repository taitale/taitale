from taitale.askap import mssplit


def test_mssplit(test_taitale_env, capfd):
    mssplit(
        env=test_taitale_env,
        args={
            "vis": "input.ms",
            "outputvis": "output.ms",
            "beams": "[0,1,2]",
            "channel": "1-300",
            "width": "1",
            "column": "DATA",
            "timebegin": "2010/12/25/00:00:00",
            "timeend": "2010/12/26/00:00:00",
            "rows": "0-1000",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """vis = input.ms
outputvis = output.ms
beams = [0,1,2]
channel = 1-300
width = 1
column = DATA
timebegin = 2010/12/25/00:00:00
timeend = 2010/12/26/00:00:00
rows = 0-1000

Starting mssplit
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mssplit -c taitale_mssplit.in \
> mssplit-2021-11-05-09-51-38.log
mssplit complete
"""
    )
