from taitale.askap import mslist


def test_mslist(test_taitale_env, capfd):
    mslist(env=test_taitale_env, args="--brief test.MS")

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting mslist
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mslist --brief test.MS \
> mslist-2021-11-05-09-51-38.log 2>&1
mslist complete
"""
    )


def test_mslist_write_to_custom_file(test_taitale_env, capfd):

    mslist(env=test_taitale_env, args="--brief test.MS", logfile="mslist.txt")

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting mslist
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mslist --brief test.MS \
> mslist.txt 2>&1
mslist complete
"""
    )
