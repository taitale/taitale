from taitale.askap import linmos


def test_linmos(test_taitale_env, capfd):
    linmos(
        env=test_taitale_env,
        args={
            "names": "['image.0', 'image.1', 'image.2']",
            "weights": "['weight.0', 'weight.1', 'weight.2']",
            "outname": "mosaic",
            "outweight": "weight",
            "weighttype": "FromWeightImages",
            "weightstate": "Inherent",
            "nterms": "2",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """linmos.names = ['image.0', 'image.1', 'image.2']
linmos.weights = ['weight.0', 'weight.1', 'weight.2']
linmos.outname = mosaic
linmos.outweight = weight
linmos.weighttype = FromWeightImages
linmos.weightstate = Inherent
linmos.nterms = 2

Starting linmos
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 linmos -c taitale_linmos.in \
> linmos-2021-11-05-09-51-38.log
linmos complete
"""
    )


def test_linmos_custom_parset_name(test_taitale_env, capfd):
    linmos(
        env=test_taitale_env,
        args={
            "names": "['image_feed00.i.restored', 'image_feed01.i.restored']",
            "weights": "['weights_feed00.i.restored', 'weights_feed01.i.restored']",
            "outname": "image_mosaic.i.dirty",
            "outweight": "weights_mosaic.i.dirty",
            "weighttype": "FromWeightImages",
            "weightstate": "Corrected",
            "imagetype": "fits",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """linmos.names = ['image_feed00.i.restored', 'image_feed01.i.restored']
linmos.weights = ['weights_feed00.i.restored', 'weights_feed01.i.restored']
linmos.outname = image_mosaic.i.dirty
linmos.outweight = weights_mosaic.i.dirty
linmos.weighttype = FromWeightImages
linmos.weightstate = Corrected
linmos.imagetype = fits

Starting linmos
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 linmos -c taitale_linmos.in > \
linmos-2021-11-05-09-51-38.log
linmos complete
"""
    )
