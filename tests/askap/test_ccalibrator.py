from taitale.askap import ccalibrator


def test_CCalibrator(test_taitale_env, capfd):
    ccalibrator(
        env=test_taitale_env,
        parset="tests/assets/CCalibratorParset.in",
        args={
            "dataset": "1934-638.ms",
            "sources.names": "['1934-638']",
            "sources.1934-638.direction": ("[19h39m25.027, -63.42.45.61, J2000]"),
            "sources.1934-638.model": "1934-638.model",
            "calibaccess": "table",
            "calibaccess.table": "calibdata.tab",
            "calibaccess.table.maxant": "36",
            "solve": "antennagains",
            "normalisegains": "true",
            "interval": "300.0s",
            "nAnt": "36",
            "refantenna": "1",
            "ncycles": "45",
            "nterms": "1",
            "gridder.snapshotimaging": "false",
            "gridder": "WProject",
            "gridder.WProject.wmax": "35000",
            "gridder.WProject.nwplanes": "257",
            "gridder.WProject.oversample": "7",
            "gridder.WProject.maxsupport": "512",
            "gridder.WProject.variablesupport": "true",
            "gridder.WProject.offsetsupport": "true",
            "gridder.WProject.sharecf": "true",
            "gridder.WProject.frequencydependent": "true",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """Ccalibrator.calibaccess = table
Ccalibrator.calibaccess_table = calibdata.tab
Ccalibrator.solve = antennagains
Ccalibrator.normalisegains = true
Ccalibrator.interval = 300.0s
Ccalibrator.nAnt = 36
Ccalibrator.refantenna = 1
Ccalibrator.ncycles = 45
Ccalibrator.nterms = 1
Ccalibrator.dataset = 1934-638.ms
Ccalibrator.sources.names = ['1934-638']
Ccalibrator.sources.1934-638.direction = [19h39m25.027, -63.42.45.61, J2000]
Ccalibrator.sources.1934-638.model = 1934-638.model
Ccalibrator.calibaccess.table = calibdata.tab
Ccalibrator.calibaccess.table.maxant = 36
Ccalibrator.gridder.snapshotimaging = false
Ccalibrator.gridder = WProject
Ccalibrator.gridder.WProject.wmax = 35000
Ccalibrator.gridder.WProject.nwplanes = 257
Ccalibrator.gridder.WProject.oversample = 7
Ccalibrator.gridder.WProject.maxsupport = 512
Ccalibrator.gridder.WProject.variablesupport = true
Ccalibrator.gridder.WProject.offsetsupport = true
Ccalibrator.gridder.WProject.sharecf = true
Ccalibrator.gridder.WProject.frequencydependent = true

Starting ccalibrator
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 ccalibrator -c taitale_ccalibrator.in \
> ccalibrator-2021-11-05-09-51-38.log
ccalibrator complete
"""
    )
