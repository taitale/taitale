from taitale import taitale_env
from taitale.askap import cddcalibrator


def test_cddcalibrator(capfd):
    env = taitale_env(runtime="native", dryrun=True, user_defaults=False)

    cddcalibrator(
        env=env,
        workers=6,
        args={
            "dataset": "observation.ms",
            "nAnt": "36",
            "nBeam": "36",
            "refantenna": "1",
            "calibaccess": "table",
            "calibaccess.table": "ddcal.tab",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """dataset = observation.ms
nAnt = 36
nBeam = 36
refantenna = 1
calibaccess = table
calibaccess.table = ddcal.tab

Starting Cddcalibrator
mpiexec -n 6 Cddcalibrator -c taitale_cddcalibrator.in > Cddcalibrator-2021-11-05-09-51-38.log
Cddcalibrator complete
"""
    )
