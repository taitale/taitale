from taitale.askap import msmerge


def test_msmerge_example_1(test_taitale_env, capfd):
    msmerge(env=test_taitale_env, args="-o fullband.ms subband_abc.ms")

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting msmerge
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 msmerge --output_file fullband.ms \
subband_abc.ms > msmerge-2021-11-05-09-51-38.log
msmerge complete
"""
    )


def test_msmerge_example_2(test_taitale_env, capfd):
    msmerge(
        env=test_taitale_env,
        args="-x 4 -c 54 -o output.ms channel1.ms channel2.ms channel3.ms",
    )

    out, _ = capfd.readouterr()

    assert (
        out
        == """Starting msmerge
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 msmerge --output_file output.ms \
--tileNcorr 4 --tileNchan 54 channel1.ms channel2.ms channel3.ms \
> msmerge-2021-11-05-09-51-38.log
msmerge complete
"""
    )
