from taitale.askap import cbpcalibrator


def test_Cbpcalibrator(test_taitale_env, capfd):
    cbpcalibrator(
        env=test_taitale_env,
        args={
            "dataset": "calibration_data.ms",
            "nChan": "304",
            "calibaccess": "table",
            "calibaccess.table": "calibdata.tab",
            "calibaccess.table.maxbeam": "9",
            "nAnt": "6",
            "nBeam": "9",
            "refantenna": "1",
            "sources.names": "['src1']",
            "sources.cal.flux.i": "1.0",
            "sources.cal.direction.ra": "0.003",
            "sources.cal.direction.dec": "0.0",
            "sources.cal.shape.bmaj": "4.848e-06",
            "sources.cal.shape.bmin": "4.848e-06",
            "sources.cal.shape.bpa": "4.848e-06",
            "sources.src1.direction": "[12h30m00.000, -45.00.00.000, J2000]",
            "sources.src1.components": "['cal']",
            "sources.cal.calibrator": "1934-638",
            "ncycles": "5",
            "gridder.snapshotimaging": "false",
            "gridder": "WProject",
            "gridder.WProject.wmax": "35000",
            "gridder.WProject.nwplanes": "257",
            "gridder.WProject.oversample": "7",
            "gridder.WProject.maxsupport": "512",
            "gridder.WProject.variablesupport": "true",
            "gridder.WProject.offsetsupport": "true",
            "gridder.WProject.sharecf": "true",
            "gridder.WProject.frequencydependent": "true",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """Cbpcalibrator.dataset = calibration_data.ms
Cbpcalibrator.nChan = 304
Cbpcalibrator.calibaccess = table
Cbpcalibrator.calibaccess.table = calibdata.tab
Cbpcalibrator.calibaccess.table.maxbeam = 9
Cbpcalibrator.nAnt = 6
Cbpcalibrator.nBeam = 9
Cbpcalibrator.refantenna = 1
Cbpcalibrator.sources.names = ['src1']
Cbpcalibrator.sources.cal.flux.i = 1.0
Cbpcalibrator.sources.cal.direction.ra = 0.003
Cbpcalibrator.sources.cal.direction.dec = 0.0
Cbpcalibrator.sources.cal.shape.bmaj = 4.848e-06
Cbpcalibrator.sources.cal.shape.bmin = 4.848e-06
Cbpcalibrator.sources.cal.shape.bpa = 4.848e-06
Cbpcalibrator.sources.src1.direction = [12h30m00.000, -45.00.00.000, J2000]
Cbpcalibrator.sources.src1.components = ['cal']
Cbpcalibrator.sources.cal.calibrator = 1934-638
Cbpcalibrator.ncycles = 5
Cbpcalibrator.gridder.snapshotimaging = false
Cbpcalibrator.gridder = WProject
Cbpcalibrator.gridder.WProject.wmax = 35000
Cbpcalibrator.gridder.WProject.nwplanes = 257
Cbpcalibrator.gridder.WProject.oversample = 7
Cbpcalibrator.gridder.WProject.maxsupport = 512
Cbpcalibrator.gridder.WProject.variablesupport = true
Cbpcalibrator.gridder.WProject.offsetsupport = true
Cbpcalibrator.gridder.WProject.sharecf = true
Cbpcalibrator.gridder.WProject.frequencydependent = true

Starting cbpcalibrator
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 cbpcalibrator -c taitale_cbpcalibrator.in \
> cbpcalibrator-2021-11-05-09-51-38.log
cbpcalibrator complete
"""
    )
