from taitale.askap import selavy


def test_selavy(test_taitale_env, capfd):
    selavy(
        env=test_taitale_env,
        workers=19,
        args={
            "image": "image.fits",
            "snrCut": "5",
            "flagGrowth": "true",
            "growthThreshold": "3",
            "flagNegative": "true",
            "thresholdType": "sigma",
            "verbose": "true",
            "nsubx": "6",
            "nsuby": "3",
            "overlapx": "10",
            "overlapy": "10",
        },
    )

    out, _ = capfd.readouterr()
    assert (
        out
        == """Selavy.image = image.fits
Selavy.snrCut = 5
Selavy.flagGrowth = true
Selavy.growthThreshold = 3
Selavy.flagNegative = true
Selavy.thresholdType = sigma
Selavy.verbose = true
Selavy.nsubx = 6
Selavy.nsuby = 3
Selavy.overlapx = 10
Selavy.overlapy = 10

Starting selavy
docker run --rm -w /home/yanda --user $UID:$GID -v $PWD:/home/yanda/ \
docker.io/csirocass/yandasoft:1.11.0-openmpi4 mpiexec -n 19 selavy -c \
taitale_selavy.in > selavy-2021-11-05-09-51-38.log
selavy complete
"""
    )
