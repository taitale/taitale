CHANGELOG
---------

master
^^^^^^

1. Add support for LOFAR losoto
2. Add support for ASKAP cddcalibrator

1.0.0
^^^^^^

1. Refactor Parset, CliApp and default handling logic
2. Add support for wsclean and DP3


0.0.2
^^^^^

1. Support cli apps
2. Add ASKAP ccontsubtract, mslist
3. Support parsets without prefix
4. Add ASKAP msmerge, mssplit, msconcat
