=============================
Taitale Cookbook
=============================

1. Imaging
--------------

.. nbgallery::
    taitale-cookbook/notebooks/1. Imaging Comparison


2. Simulations
--------------

.. nbgallery::
    taitale-cookbook/notebooks/2. Simulating a MS from a VOTable

3. Source Detection
-------------------

.. nbgallery::
    taitale-cookbook/notebooks/3. Continuum Source Detection

4. Mosaicking
--------------

.. nbgallery::
    taitale-cookbook/notebooks/4. Mosaicking with Linmos
