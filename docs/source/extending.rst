=================
Extending Taitale
=================

Adding New Tools
----------------

Taitale provides two types of tool wrappers:

1. ``parset_app``: For tools that use parameter sets (parsets)
2. ``cli_app``: For tools that use command-line arguments

Creating a Parset-based Tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here's how to create a new tool that uses parameter sets:

.. code-block:: python

    from typing import Union
    from taitale.utils.Parset import Parset
    from taitale.utils.wrapper import parset_app

    def my_tool(
        parset: Union[str, Parset, None] = None,
        **kwargs,
    ):
        # Initialize parset
        if parset is None:
            parset = Parset(prefix="mytool")
        elif isinstance(parset, str):
            parset_obj = Parset(prefix="mytool")
            parset_obj.read_from_file(parset)
            parset = parset_obj
        elif not isinstance(parset, Parset):
            raise TypeError("parset must be either a string path or a Parset instance")

        # Create the tool wrapper
        my_tool_app = parset_app(
            name="mytool",
            parset=parset,
            out_parset_name="taitale_mytool.in",
            cmd="mytool -c {parset_name}",
            mpi_compartible=True,  # Set to True if tool supports MPI
        )

        # Execute the tool
        my_tool_app(**kwargs)

    # Add docstring with usage examples
    my_tool.__doc__ = """
    My custom tool description

    Parameters
    ----------
    parset : str or Parset or None, optional
        Either a path to a parset file, a Parset instance, or None to create a new Parset
    **kwargs : dict
        Additional keyword arguments passed to the tool

    Example
    -------
    >>> from taitale.mytools import my_tool
    >>> my_tool(
            args={
                "param1": "value1",
                "param2": "value2",
            }
        )
    """

Creating a CLI-based Tool
~~~~~~~~~~~~~~~~~~~~~~~~~

For tools that use command-line arguments:

.. code-block:: python

    from taitale.utils.wrapper import cli_app

    def my_cli_tool(**kwargs):
        # Create the tool wrapper
        my_tool_app = cli_app(
            name="mytool",
            cli_args="",  # Default CLI arguments if any
            cmd="mytool {args}",  # Command template
            mpi_compartible=True,  # Set to True if tool supports MPI
        )

        # Execute the tool
        my_tool_app(**kwargs)

    # Add docstring with usage examples
    my_cli_tool.__doc__ = """
    My custom CLI tool description

    Parameters
    ----------
    **kwargs : dict
        Command line arguments passed to the tool

    Example
    -------
    >>> from taitale.mytools import my_cli_tool
    >>> my_cli_tool(
            args="-i input.ms -o output.ms"
        )
    """
