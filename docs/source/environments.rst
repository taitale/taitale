=================
Environments
=================

The ``taitale_env`` function provides a flexible way to configure the runtime environment. It supports both container-based and native execution, along with different workload managers.

Basic Usage
-----------

.. code-block:: python

    from taitale import taitale_env

    # Default: container runtime with docker engine
    env = taitale_env()

    # Native runtime
    env = taitale_env(runtime='native')

    # Container runtime with podman
    env = taitale_env(runtime='container', engine='podman')

Configuration Options
---------------------

The environment can be configured with the following options:

* ``runtime``: Environment to run ASKAP tools ('container' or 'native', default: 'container')
* ``image``: Container image to use (default: 'registry.gitlab.com/askapsdp/all_yandasoft')
* ``tag``: Container image tag (default: '1.1.x-latest-slim')
* ``engine``: Container engine to use ('docker' or 'podman', default: 'docker')
* ``workload_manager``: Workload manager for distribution ('mpi' or 'slurm', default: 'mpi')
* ``path``: Path to executable (default: None)
* ``dryrun``: Print parset to stdout without execution (default: False)
* ``user_defaults``: Read defaults from configuration files (default: True)

Container Environment
---------------------

When using containers (default):

.. code-block:: python

    from taitale import taitale_env

    # Use a specific image tag
    env = taitale_env(
        runtime='container',
        image='docker.io/csirocass/askapsoft',
        tag='1.1.x'
    )

    # Use podman instead of docker
    env = taitale_env(
        runtime='container',
        engine='podman'
    )

Native Environment
------------------

For native execution:

.. code-block:: python

    from taitale import taitale_env

    # Basic native execution
    env = taitale_env(runtime='native')

    # Native with SLURM workload manager
    env = taitale_env(
        runtime='native',
        workload_manager='slurm'
    )

Configuration Files
-------------------

You can set default configurations in files to avoid specifying them every time:

1. Package defaults: ``taitale/taitale_env.cfg``
2. User defaults: ``~/.taitale/taitale_env.cfg``
3. Local defaults: ``./taitale_env.cfg``

The configuration files are read in the above order, with later files overriding earlier ones.
Configuration file format:

.. code-block:: ini

    # taitale_env.cfg
    [taitale_env]
    runtime = container
    engine = docker
    image = docker.io/csirocass/askapsoft
    tag = 1.17.6-openmpi4
    workload_manager = mpi

    [mpi]
    workload_exec = mpiexec -n {workers} {cmd}

    [slurm]
    workload_exec = srun -n {workers} {cmd}

    [native]
    path = /usr/local/bin

    [container]
    path = /usr/local/bin

Error Handling
--------------

The environment setup will raise ``ValueError`` if:

* Invalid runtime is specified (must be 'container' or 'native')
* Invalid workload manager is specified (must be 'mpi' or 'slurm')

Example:

.. code-block:: python

    # This will raise ValueError
    env = taitale_env(runtime='invalid')  # ValueError: Unknown environment

    # This will also raise ValueError
    env = taitale_env(workload_manager='invalid')  # ValueError: Unknown workload_manager
