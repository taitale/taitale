Welcome to taitale's documentation!
===================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   getting-started
   tools
   parsets
   environments
   extending
   casa
   cookbook
   faq

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
