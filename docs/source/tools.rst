=============================
Taitale tools
=============================

This documentation covers the various tools available in Taitale:

.. toctree::
   :maxdepth: 2

   lofar_tools
   askap_tools


Tools
=====

The tools module provides wrappers around radio astronomy tools.


Parset-based Tools
------------------

Many tools (like csimulator, linmos, selavy) use parameter sets (parsets) to configure their behavior.
These tools accept the following common parameters:

* ``parset`` (Union[str, Parset, None]): Either a path to a parset file, a Parset instance, or None to create a new Parset
* ``args`` (dict): Dictionary of parameters to pass to the tool
* ``workers`` (Optional[int]): Number of MPI workers to use for parallel processing

The ``args`` dictionary should contain string values for all parameters. For example:

.. code-block:: python

    args = {
        "dataset": "input.ms",
        "nchan": "16416",
        "singleoutputms": "true"
    }

CLI-based Tools
---------------

Some tools (like msconcat, mslist, wsclean) use command-line arguments instead of parsets.
These tools accept the following common parameters:

* ``args`` (str): A string of command-line arguments
* ``workers`` (Optional[int]): Number of MPI workers to use for parallel processing (if supported)

The ``args`` can be provided as:

.. code-block:: python

    args = "-o output.ms input1.ms input2.ms input3.ms"

Usage Examples
--------------

Tools using parsets
~~~~~~~~~~~~~~~~~~~

The tools in taitale can be used in several ways:

1. Using direct keyword arguments:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import linmos

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")

    linmos(
        env=env,
        args={
            "names": "['image_1', 'image_2', 'image_3']",
            "weighttype": "FromPrimaryBeamModel",
            "outname": "mosaic.out",
            "outweight": "mosaic.weight.out",
        },
    )


2. Using an existing parset file:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import ccalapply

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")
    ccalapply(
        env=env,
        parset="ccalapply.in",
        args={
            "dataset": "1934-638.ms",
            "calibaccess.table": "1934-638.calib.tab",
        },
    )

3. Using a parset instance:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import ccalapply
    from taitale.utils import Parset

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")

    # Create a parset instance
    parset = Parset("Ccalapply")
    parset.set("dataset", "1934-638.ms")
    parset.set("calibrate.scalenoise", "true")
    parset.set("calibrate.allowflag", "false")
    parset.set("calibaccess", "table")
    parset.set("calibaccess.table", "1934-638.calib.tab")
    parset.set("calibaccess.table.maxbeam", "30")

    ccalapply(parset=parset)

Tools using CLI arguments
~~~~~~~~~~~~~~~~~~~~~~~~~

For tools that use command-line arguments:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import mslist, msconcat

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")

    # Simple tool with flags
    mslist(
        env=env,
        args="--brief test.MS",
        logfile="mslist.txt"
    )

    # Tool with multiple input files
    msconcat(
        env=env,
        args="-o output.ms input1.ms input2.ms input3.ms"
    )

Notes
-----

1. All parameter values in the ``args`` dictionary must be strings
2. Lists should be passed as string representations: ``"[1, 2, 3]"``
3. Boolean values should be passed as strings: ``"true"`` or ``"false"``
4. The ``workers`` parameter controls MPI parallelization for supported tools
5. Custom parset names can be specified using ``out_parset_name``
