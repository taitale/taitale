=============
ASKAP Tools
=============

The ASKAP tools module provides wrappers around ASKAPsoft tools for data processing and imaging.

Calibration Tools
----------------

CCaLibrator
~~~~~~~~~~
Tool for solving gain calibration parameters.

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import ccalibrator

    env = taitale_env(runtime="container", image="csirocass/askapsoft")

    ccalibrator(
        env=env,
        args={
            "dataset": "calibrator.ms",
            "nAnt": "36",
            "nBeam": "36",
            "refantenna": "1",
            "calibaccess": "table",
            "calibaccess.table": "caldata.tab",
        },
    )

CddCalibrator
~~~~~~~~~~~~
Tool for direction-dependent calibration.

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import cddcalibrator

    env = taitale_env(runtime="container", image="csirocass/askapsoft")

    cddcalibrator(
        env=env,
        workers=6,  # Use 6 MPI workers
        args={
            "dataset": "observation.ms",
            "nAnt": "36",
            "nBeam": "36",
            "refantenna": "1",
            "calibaccess": "table",
            "calibaccess.table": "ddcal.tab",
        },
    )

CCaLApply
~~~~~~~~~
Tool for applying calibration solutions.

.. code-block:: python

    from taitale.askap import ccalapply

    ccalapply(
        env=env,
        args={
            "dataset": "target.ms",
            "calibaccess": "table",
            "calibaccess.table": "caldata.tab",
            "calibrate.scalenoise": "true",
        },
    )

CBPCalibrator
~~~~~~~~~~~~
Tool for solving bandpass calibration parameters.

.. code-block:: python

    from taitale.askap import cbpcalibrator

    cbpcalibrator(
        env=env,
        args={
            "dataset": "bandpass.ms",
            "nAnt": "36",
            "nBeam": "36",
            "refantenna": "1",
            "calibaccess": "table",
            "calibaccess.table": "bandpass.tab",
        },
    )

Imaging Tools
------------

Imager
~~~~~~
Main ASKAPsoft imaging tool.

.. code-block:: python

    from taitale.askap import imager

    imager(
        env=env,
        args={
            "dataset": "observation.ms",
            "imagetype": "fits",
            "restore": "true",
            "restore.beam": "[30arcsec, 30arcsec, 0deg]",
            "nchannel": "1",
            "frequency": "1.4GHz",
        },
    )

CModel
~~~~~~
Tool for model prediction.

.. code-block:: python

    from taitale.askap import cmodel

    cmodel(
        env=env,
        args={
            "dataset": "observation.ms",
            "sources.names": "['src1', 'src2']",
            "sources.src1.direction": "[12h30m00.00, -45.00.00.00, J2000]",
            "sources.src1.model": "point",
            "sources.src1.flux.i": "1.0",
        },
    )

Linmos
~~~~~~
Tool for linear mosaicing of images.

.. code-block:: python

    from taitale.askap import linmos

    linmos(
        env=env,
        args={
            "names": "['image_1', 'image_2', 'image_3']",
            "weighttype": "FromPrimaryBeamModel",
            "outname": "mosaic.fits",
            "outweight": "weights.fits",
        },
    )

Selavy
~~~~~~
Source finding and characterization tool.

.. code-block:: python

    from taitale.askap import selavy

    selavy(
        env=env,
        args={
            "image": "image.fits",
            "snrcut": "5",
            "threshold": "0.1mJy",
            "verbose": "true",
            "writeCatalog": "true",
            "catalogFile": "catalog.txt",
        },
    )

Measurement Set Tools
-------------------

MSConcat
~~~~~~~~
Tool for concatenating measurement sets.

.. code-block:: python

    from taitale.askap import msconcat

    msconcat(
        env=env,
        args="-o output.ms input1.ms input2.ms input3.ms"
    )

MSList
~~~~~~
Tool for listing measurement set metadata.

.. code-block:: python

    from taitale.askap import mslist

    mslist(
        env=env,
        args="--brief observation.ms",
        logfile="mslist.log"
    )

MSMerge
~~~~~~~
Tool for merging measurement sets.

.. code-block:: python

    from taitale.askap import msmerge

    msmerge(
        env=env,
        args={
            "vis": "['vis1.ms', 'vis2.ms']",
            "out": "merged.ms",
            "mode": "channel",
        },
    )

MSSplit
~~~~~~~
Tool for splitting measurement sets.

.. code-block:: python

    from taitale.askap import mssplit

    mssplit(
        env=env,
        args={
            "vis": "input.ms",
            "outputvis": "output.ms",
            "channel": "1-64",
            "width": "4",
        },
    )

Simulation Tools
--------------

CSimulator
~~~~~~~~~
Tool for simulating visibilities.

.. code-block:: python

    from taitale.askap import csimulator

    csimulator(
        env=env,
        args={
            "Csimulator.dataset": "sim.ms",
            "Csimulator.sources.names": "['src1']",
            "Csimulator.sources.src1.direction": "[12h30m00.00, -45.00.00.00, J2000]",
            "Csimulator.sources.src1.model": "point",
            "Csimulator.telescope": "ASKAP",
            "Csimulator.synthesis": "4h",
        },
    )

CContSubtract
~~~~~~~~~~~~
Tool for continuum subtraction.

.. code-block:: python

    from taitale.askap import ccontsubtract

    ccontsubtract(
        env=env,
        args={
            "dataset": "input.ms",
            "datacolumn": "DATA",
            "modelcolumn": "MODEL_DATA",
            "outputcolumn": "CORRECTED_DATA",
        },
    )

For more detailed information about ASKAPsoft tools, refer to the `ASKAPsoft documentation <https://www.atnf.csiro.au/computing/software/askapsoft/sdp/docs/>`_.
