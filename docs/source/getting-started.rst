=================
Getting Started
=================

About
-----

**taitale** provides convenient Python wrappers for radio astronomy tools, enabling their seamless integration into Python scripts and Jupyter notebooks. This integration facilitates rapid prototyping and easy comparison between different tools.

Installation
------------

Create an environment in conda

.. code-block:: shell

    conda create --name taitale
    git clone https://gitlab.com/taitale/taitale.git
    cd taitale
    poetry install

Example workflows can be found in the cookbook section of this documentation

===========
Basic Usage
===========

Tools using parsets
--------------------

The tools in taitale can be used in a couple of ways. Some examples

1. Using direct keyword arguments:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import linmos

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")

    linmos(
        env=env,
        args={
            "names": "['image_1', 'image_2', 'image_3']",
            "weighttype": "FromPrimaryBeamModel",
            "outname": "mosaic.out",
            "outweight": "mosaic.weight.out",
        },
    )


2. Using an existing parset file, but overriding necessary arguments:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import ccalapply

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")
    ccalapply(
        env=env,
        parset="ccalapply.in",
        args={
            "dataset": "1934-638.ms",
            "calibaccess.table": "1934-638.calib.tab",
        },
    )

3. Using a parset instance:

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import ccalapply
    from taitale.utils import Parset

    env = taitale_env(runtime="container", image="csirocass/askapsoft", tag="1.17.6-openmpi4")

    # Create a parset instance
    parset = Parset("Ccalapply")
    parset.set("dataset", "1934-638.ms")
    parset.set("calibrate.scalenoise", "true")
    parset.set("calibrate.allowflag", "false")
    parset.set("calibaccess", "table")
    parset.set("calibaccess.table", "1934-638.calib.tab")
    parset.set("calibaccess.table.maxbeam", "30")

    ccalapply(parset=parset)

Tools using cli args
--------------------

.. code-block:: python

    from taitale import taitale_env
    from taitale.askap import mslist
    mslist(env=test_taitale_env, args="--brief test.MS", logfile="mslist.txt")

Roadmap
-------

* Usage with Slurm
* Composibility of tools
