======================
FAQ & Common Issues
======================

This page lists frequently asked questions and common issues that users might encounter when using Taitale.

Container Volume Mounting
-----------------------

Working Directory Limitations
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When using Taitale with container runtimes (Docker/Podman), only the current working directory is mounted inside the container.
This means that only files and directories within your current working directory will be accessible to the tools running inside the container.

Example:
    If you run Taitale from ``/home/user/project``, only files within this directory will be accessible to the containerized tools.
    Files outside this directory (e.g., ``/home/user/data``) will not be accessible unless you:
    
    1. Change your working directory to include all required files
    2. Create symbolic links to the required files within your working directory
    3. Modify the container configuration to mount additional directories

Podman SELinux Considerations
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When using Podman on systems with SELinux enabled (common on Red Hat-based distributions), you need to use the ``:z`` suffix
with volume mounts to handle SELinux contexts correctly.

.. code-block:: python

    taitale_env(
    runtime="container\",
    image="docker.io/csirocass/askapsoft",
    tag="1.17.6-openmpi4",
    engine="podman"
    container={'exec': '{engine} run --rm -v $PWD:/wsclean/:z {image}:{tag} {workload_exec} > {logfile}{pipestderr}'},
    )"

This allows the container to properly access the mounted directory while maintaining SELinux security policies.
