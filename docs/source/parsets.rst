===============
Parsets
===============

Using Parsets
=============

The ``Parset`` class provides a flexible way to create and manage parameter sets for radio astronomy tools.
You can create parsets with or without prefixes, organize parameters in groups, and easily serialize them to files.

Basic Usage
----------

1. Creating a simple parset:

.. code-block:: python

    from taitale.utils import Parset

    # Create a parset without prefix
    parset = Parset()
    parset.set("msin", "test.MS")
    parset.set("steps", "[averager, mystep1]")
    parset.set("mystep1.type", "averager")

    print(parset.to_string())
    # Output:
    # msin = test.MS
    # steps = [averager, mystep1]
    # mystep1.type = averager

2. Using prefixes and groups:

.. code-block:: python

    from taitale.utils import Parset

    # Create a parset with prefix
    parset = Parset(prefix="Cimager")
    parset.set("gridder", "WProject")

    # Use groups for nested parameters
    wproj = parset.group("gridder", "WProject")
    wproj("wmax", "35000")
    wproj("nwplanes", "257")
    wproj("oversample", "7")

    print(parset.to_string())
    # Output:
    # Cimager.gridder = WProject
    # Cimager.gridder.WProject.wmax = 35000
    # Cimager.gridder.WProject.nwplanes = 257
    # Cimager.gridder.WProject.oversample = 7

3. Reading from and writing to files:

.. code-block:: python

    from taitale.utils import Parset

    # Create and write a parset
    parset = Parset(prefix="sources")
    parset.set("names", "1934-638")
    parset.set("1934-638.direction", "[19h39m25.0342, -63.42.45.623, J2000]")
    parset.serialize("sources.in")

    # Read an existing parset file
    new_parset = Parset(prefix="sources")
    new_parset.read_from_file("sources.in")

4. Adding comments and merging parsets:

.. code-block:: python

    from taitale.utils import Parset

    # Add comments to document your parset
    parset1 = Parset(prefix="Cimager")
    parset1.comment("Configure gridder settings")
    parset1.set("gridder", "WProject")
    parset1.set("gridder.WProject.wmax", "35000")

    # Create another parset
    parset2 = Parset(prefix="Cimager")
    parset2.set("solver", "Clean")
    parset2.set("solver.Clean.niter", "1000")

    # Merge parsets
    parset1.merge(parset2)
    print(parset1.to_string())
    # Output:
    # # Configure gridder settings
    # Cimager.gridder = WProject
    # Cimager.gridder.WProject.wmax = 35000
    # Cimager.solver = Clean
    # Cimager.solver.Clean.niter = 1000

Features
--------

The ``Parset`` class provides several useful features:

* **Prefixes**: Add a prefix to all parameters (e.g., "Cimager." for imager parameters)
* **Groups**: Organize nested parameters using the ``group()`` method
* **Comments**: Add comments to document your parameter sets
* **File I/O**: Read from and write to parset files
* **Merging**: Combine multiple parsets into one
* **Type Handling**: Automatically handles boolean values ("true"/"false")
