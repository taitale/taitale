=============================
Usage with CASA
=============================

You can use yandosoft and CASA together with 

* Install taitale in the standalone python provided by CASA

.. code-block:: sh

    cd taitale
    /path/to/casa/casa-6.x/bin/pip3 install -e .


* Create a Jupyter kernel spec for the python in CASA

.. code-block:: json

    {
        "argv": [
            "/path/to/casa/casa-6.x/bin/python3",
            "-m",
            "ipykernel_launcher",
            "-f",
            "{connection_file}"
        ],
        "display_name": "CASA Python 3 (ipykernel)",
        "language": "python",
        "metadata": {
            "debugger": true
        }
    }

* Start Jupyter as you normally would and use the new python kernel we created for CASA.
