=============
LOFAR Tools
=============

The LOFAR tools module provides wrappers around common LOFAR data processing tools.

DP3
---

DP3 (DPPP - Default Pre-Processing Pipeline) is used for various operations on Measurement Sets including:
averaging, flagging, calibration, and more.

.. code-block:: python

    from taitale import taitale_env
    from taitale.lofar import dp3

    env = taitale_env(runtime="native")

    # Using direct arguments
    dp3(
        env=env,
        args={
            "msin": "input.ms",
            "steps": "[average]",
            "average.type": "averager",
            "average.timestep": "8",
            "msout": "output.ms",
            "msout.datacolumn": "DATA",
        },
    )

    # Using a parset file
    dp3(
        env=env,
        parset="dp3.parset",
    )

For more information on DP3, see the `DP3 documentation <https://dp3.readthedocs.io/>`_.

LoSoTo
------

LoSoTo (LOFAR Solution Tool) is a tool for processing and manipulating solution tables generated during calibration.

.. code-block:: python

    from taitale import taitale_env
    from taitale.lofar import losoto

    env = taitale_env(runtime="native")

    # Plot amplitude solutions
    losoto(
        env=env,
        h5parm="solutions.h5",  # H5Parm file containing solutions
        args={
            "soltab": "sol000/amplitude000",
            "operation": "PLOT",
            "axisInTable": "time",
            "axisInCol": "ant",
            "plotFlag": "True",
            "prefix": "amp_",
        },
    )

    # Smooth solutions using a parset file
    losoto(
        env=env,
        h5parm="solutions.h5",
        parset="losoto_smooth.parset",
    )

For more information on LoSoTo, see:
    - `LoSoTo GitHub repository <https://github.com/revoltek/losoto>`_
    - `LOFAR Imaging Cookbook <https://support.astron.nl/LOFARImagingCookbook/losoto.html>`_

WSClean
-------

WSClean (w-Stacking Clean) is an advanced radio interferometric imager.

.. code-block:: python

    from taitale import taitale_env
    from taitale.lofar import wsclean

    env = taitale_env(runtime="native")

    # Basic imaging
    wsclean(
        env=env,
        args="-size 1024 1024 -scale 1asec input.ms"
    )

    # Advanced imaging with multiple parameters
    wsclean(
        env=env,
        args=(
            "-size 2048 2048 -scale 0.5asec "
            "-niter 1000000 -auto-threshold 0.3 "
            "-auto-mask 3 -mgain 0.8 input.ms"
        )
    )

For more information on WSClean, see the `WSClean documentation <https://wsclean.readthedocs.io/>`_.
